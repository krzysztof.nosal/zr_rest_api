package json;

public class AlbumsResults {
    public Albums albums;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        int i=0;
        for(Album album:albums.items){
            stringBuilder.append("id: ").append(i).append("\n");
            stringBuilder.append("title: "+album.name).append("\n");
            stringBuilder.append("number of tracks: "+album.total_tracks).append("\n");
            stringBuilder.append("//////\n");
            i++;
        }
        return stringBuilder.toString();
    }
}
