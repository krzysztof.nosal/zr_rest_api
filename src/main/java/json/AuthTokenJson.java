package json;

public class AuthTokenJson {
    private String access_token;
    private String scope;
    private String token_type;
    private int expires_in;

    public AuthTokenJson() {
    }

    public String getAccess_token() {
        return access_token;
    }
}
