package json;

public class Device {
    public String id;
    public boolean is_active;
    public boolean is_private_session;
    public boolean is_restricted;
    public String name;
    public String type;
    public int volume_percent;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
       // stringBuilder.append("id: ").append(id).append("\n");
        stringBuilder.append("name: ").append(name).append("\n");
        stringBuilder.append("type: ").append(type).append("\n");
        return stringBuilder.toString();
    }
}
