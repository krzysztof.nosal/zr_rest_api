package json;

import java.lang.reflect.Array;

public class Devices {
  public Device[]devices;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        int i=0;
        for(Device device: devices){
            stringBuilder.append("id: ").append(i).append("\n");
            stringBuilder.append(device.toString()).append("\n");
            stringBuilder.append("////\n");
            i++;
        }
        return stringBuilder.toString();
    }
}
