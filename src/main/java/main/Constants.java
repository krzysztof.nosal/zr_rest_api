package main;

public class Constants {
   private static String apiCode;
   private static String applicationId;
   private static String applicationSecret;
   public static final String LOCALHOST = "http://localhost";

   static {
      applicationId="bd87dc2b29dc40e393948b0141c61ff3";
      applicationSecret="9aadf3db993c4d44a37821a92451157d";
   }

   public static String getApiCode() {
      return apiCode;
   }

   public static void setApiCode(String apiCode) {
      Constants.apiCode = apiCode;
   }

   public static String getApplicationId() {
      return applicationId;
   }

   public static void setApplicationId(String applicationId) {
      Constants.applicationId = applicationId;
   }

   public static String getApplicationSecret() {
      return applicationSecret;
   }

   public static void setApplicationSecret(String applicationSecret) {
      Constants.applicationSecret = applicationSecret;
   }
}
