package main;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;


public class LocalHttpServer {
    private final com.sun.net.httpserver.HttpServer httpServer;
    private final Object interThreadLock;

    public LocalHttpServer(Object interThreadLock) throws IOException {
        this.interThreadLock = interThreadLock;
        HttpServer server;
        server = HttpServer.create();
        server.bind(new InetSocketAddress(8080), 0);
        var authServerHandler = new AuthServerHandler(interThreadLock);
        server.createContext("/",authServerHandler);
        httpServer = server;
    }

    public void start() {
        httpServer.start();
    }

    public void stop() {
        httpServer.stop(1);
    }
}
