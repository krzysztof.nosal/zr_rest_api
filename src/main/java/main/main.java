package main;


import java.util.Scanner;

public class main {

    public static void main(String[] args) {

        var spotifyClient = new SpotifyClient();
        Scanner scanner = new Scanner(System.in);
        spotifyClient.getAlbums(scanner.nextLine());
        System.out.println("type id of chosen album:");
        int albumId = scanner.nextInt();
        System.out.println("below list of available devices:");
        spotifyClient.getListOfDevices();
        System.out.println("type id of chosen device:");
        spotifyClient.playAlbum(albumId,scanner.nextInt());
    }

}

