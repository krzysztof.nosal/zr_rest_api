package main;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;

import java.util.regex.Pattern;

public class AuthServerHandler implements HttpHandler {
    private final Object interThreadLock;

    public AuthServerHandler(Object interThreadLock) {
        this.interThreadLock = interThreadLock;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        var response = "Authorization error";
        String query = exchange.getRequestURI().getQuery();
        var compiledPattern = Pattern.compile("(?<=code=)[^&%]+");
        var matcher = compiledPattern.matcher(query);
        if (matcher.find()){
            Constants.setApiCode(matcher.group());
            response = "Logged in, go back to your app";
        }
        exchange.sendResponseHeaders(200, response.length());
        exchange.getResponseBody().write(response.getBytes());
        exchange.getResponseBody().close();
        synchronized (interThreadLock) {
            interThreadLock.notifyAll();
        }
    }
}
