package main;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import json.*;

import java.awt.*;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Base64;

public class SpotifyClient {
    private Devices devices;
    private AlbumsResults albumsResults;

    public SpotifyClient() {
        try {
            getAdvancedApiCode();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getBasicApiCode() {
        String originalInput = Constants.getApplicationId() + ":" + Constants.getApplicationSecret();
        var encodedString = Base64.getEncoder().encodeToString(originalInput.getBytes());
        HttpResponse<JsonNode> jsonResponse = null;
        try {
            jsonResponse = Unirest.post("https://accounts.spotify.com/api/token")
                    .header("Authorization", "Basic " + encodedString)
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .field("grant_type", "client_credentials")
                    .asJson();
        } catch (Exception e) {
            e.printStackTrace();
        }
        var gson = new Gson();
        if (jsonResponse != null) {
            tokenJson token = gson.fromJson(jsonResponse.getBody().toString(), new TypeToken<tokenJson>() {
            }.getType());
            String authToken = token.getAccess_token();
            Constants.setApiCode(authToken);
            if (token.getAccess_token() != null) {
                return authToken;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public String getAdvancedApiCode() throws IOException, URISyntaxException, InterruptedException {
        var interThreadLock = new Object();
        LocalHttpServer server = null;
        server = new LocalHttpServer(interThreadLock);

        //first stage of authorization
        server.start();

        String url = "https://accounts.spotify.com/authorize?" +
                "client_id=" + "bd87dc2b29dc40e393948b0141c61ff3" +
                "&redirect_uri=" + Constants.LOCALHOST + ":8080" +
                "&response_type=code" +
                "&scope=user-read-playback-state%20user-modify-playback-state";

        var desktop = Desktop.getDesktop();
        desktop.browse(new URI(url));

        synchronized (interThreadLock) {
            interThreadLock.wait();
            System.in.read(new byte[System.in.available()]);
        }
        server.stop();

        //second stage of authorization after temp code over HTTP is received by local http server
        HttpResponse<JsonNode> jsonResponse = null;

        try {
            jsonResponse = Unirest.post("https://accounts.spotify.com/api/token")
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .field("grant_type", "authorization_code")
                    .field("code", Constants.getApiCode())
                    .field("client_id", "bd87dc2b29dc40e393948b0141c61ff3")
                    .field("client_secret", "9aadf3db993c4d44a37821a92451157d")
                    .field("redirect_uri", "http://localhost:8080")
                    .field("scope", "user-read-playback-state%20user-modify-playback-state")
                    .asJson();
        } catch (Exception e) {
            e.printStackTrace();
        }
        var gson = new Gson();
        AuthTokenJson authTokenJson;
        if (jsonResponse != null) {
            authTokenJson = gson.fromJson(jsonResponse.getBody().toString(), new TypeToken<AuthTokenJson>() {
            }.getType());

            String authToken = authTokenJson.getAccess_token();
            Constants.setApiCode(authToken);
            System.out.println("logged in successfully");
            System.out.println("type in artist name");
            return authToken;
        } else {
            System.out.println("error while logging");
            return null;
        }

    }

    public void getListOfDevices() {
        HttpResponse<JsonNode> jsonResponse = null;

        try {
            jsonResponse = Unirest.get("https://api.spotify.com/v1/me/player/devices")
                    .header("Authorization", "Bearer " + Constants.getApiCode())
                    .header("Content-Type", "application/json")
                    .asJson();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Type type = new TypeToken<Devices>() {
        }.getType();
        Gson gson = new Gson();
        this.devices = gson.fromJson(jsonResponse.getBody().toString(), type);

        System.out.println(devices);
    }

    public void getAlbums(String artist){

        HttpResponse<JsonNode> jsonResponse = null;
        try {
            jsonResponse = Unirest.get("https://api.spotify.com/v1/search")
                    .header("Authorization", "Bearer " + Constants.getApiCode())
                    .header("Content-Type", "application/json").queryString("q", "artist:" + artist).queryString("type", "album")
                    .asJson();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Type type = new TypeToken<AlbumsResults>() {
        }.getType();
        Gson gson = new Gson();
        albumsResults = gson.fromJson(jsonResponse.getBody().toString(), type);
        System.out.println(albumsResults.toString());
    }

    public void playAlbum(int albumNo,int deviceNo){
        try {
            Unirest.put("https://api.spotify.com/v1/me/player/play")
                    .header("Authorization", "Bearer " + Constants.getApiCode())
                    .header("Content-Type", "application/json")
                    .queryString("device_id",devices.devices[deviceNo].id)
                    .body("{\n" +
                            "  \"context_uri\": \"spotify:album:"+albumsResults.albums.items[albumNo].id+"\",\n" +
                            "  \"offset\": {\n" +
                            "    \"position\": 5\n" +
                            "  },\n" +
                            "  \"position_ms\": 0\n" +
                            "}")
                    .asJson();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
